//
//  InterfaceController.swift
//  watchSushi WatchKit Extension
//
//  Created by chandra keerthi on 2019-10-30.
//  Copyright © 2019 student. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController,WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    var msg :String!
    var name:String!
    @IBOutlet weak var counterLabel: WKInterfaceLabel!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        super.awake(withContext: context)
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        // Configure interface objects here.
        
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        WKInterfaceDevice().play(.click)
        self.msg = message["counter"] as! String
        if (self.msg == "15"){
            counterLabel.setText(self.msg)
               let action = WKAlertAction(title: "Decline", style: WKAlertActionStyle.default) {
                   
               }
               presentAlert(withTitle: "Play Fast", message: "10 Seconds Finished", preferredStyle: WKAlertControllerStyle.alert, actions:[action])

               }
        else if (self.msg == "10"){
            counterLabel.setText(self.msg)
               let action = WKAlertAction(title: "Decline", style: WKAlertActionStyle.default) {
                   
               }
               presentAlert(withTitle: "Play Fast", message: "10 Seconds remaining", preferredStyle: WKAlertControllerStyle.alert, actions:[action])
               
               }
        else if(self.msg == "5"){
            counterLabel.setText(self.msg)
               let action = WKAlertAction(title: "Decline", style: WKAlertActionStyle.default) {
                   
               }
               presentAlert(withTitle: "Play Fast", message: "5 Seconds remaining", preferredStyle: WKAlertControllerStyle.alert, actions:[action])
               }
    
    }

    @IBAction func leftPressed() {
            if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            WCSession.default.sendMessage(
                ["position":"left"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    
                    print("Message sent, put something here if u are expecting a reply from the phone")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
            })
        }
        else {
            print("Phone is not reachable")
                }
            
               
    }
               
    @IBAction func rightPressed() {
          if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            WCSession.default.sendMessage(
                ["position":"right"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent, put something here if u are expecting a reply from the phone")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
            })
        }
        else {
            print("Phone is not reachable")
                }
            
               
    }
    @IBAction func pauseButtonPressed() {
        
        
       let suggestedResponses = ["keerthi", "emma","bella","micheal"]
        presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) {
            
            (results) in
            
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                
                self.name = userResponse
            }
        }
        
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            WCSession.default.sendMessage(
                ["name":"\(self.name)"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent, put something here if u are expecting a reply from the phone")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
            })
        }
        else {
            print("Phone is not reachable")
                }
    }
}
